$flights = Flight::where('active', 1)
               ->orderBy('name')
               ->take(10)
               ->get();

// fresh model
$flight = Flight::where('number', 'FR 900')->first();
$freshFlight = $flight->fresh();

// refreshing model
$flight = Flight::where('number', 'FR 900')->first();
$flight->number = 'FR 456';
$flight->refresh();
$flight->number; // "FR 900"



# one to one 
class User extends Model
{
    /**
     * Get the phone associated with the user.
     */
    public function phone()
    {
        return $this->hasOne(Phone::class); // return $this->hasOne(Phone::class, 'foreign_key', 'local_key); .. here foreign_key=>user_id, local_key=>id as convention.. 
    }
}

# $phone = User::find(1)->phone;

# inverse of one to one 
class Phone extends Model
{
    /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo(User::class); // return $this->belongsTo(User::class, 'foreign_key', 'owner_key'); ....[user_id, id]
    }
}


# one to many
class Post extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class); // post_id, id // return $this->hasMany(Comment::class, 'foreign_key', 'local_key');
    }
}

# access one to many
$comments = Post::find(1)->comments;
foreach ($comments as $comment) {
    //
}

# access one to many with condition
$comment = Post::find(1)->comments()
                    ->where('title', 'foo')
                    ->first();

# one to many inverse..
class Comment extends Model
{
    public function post()
    {
        return $this->belongsTo(Post::class); // return $this->belongsTo(Post::class, 'foreign_key', 'owner_key');
    }
}
# comment post
$comment = Comment::find(1);
$postTitle = $comment->post->title;


# many to many....
users
    id - integer
    name - string

roles
    id - integer
    name - string

role_user
    user_id - integer
    role_id - integer

# User model
class User extends Model
{
    public function roles()
    {
        return $this->belongsToMany(Role::class); // return $this->belongsToMany(Role::class, 'role_user'); // return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }
}

hint: extra attribute in pivot table
return $this->belongsToMany(Role::class)->withPivot('active', 'created_by');
return $this->belongsToMany(Role::class)->withTimestamps();

# Role model 
class Role extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}

# access user roles
$user = User::find(1);
foreach ($user->roles as $role) {
    //
}

// $roles = User::find(1)->roles()->orderBy('name')->get();

# Retrieving Intermediate Table Columns

$user = User::find(1);

foreach ($user->roles as $role) {
    echo $role->pivot->created_at;
}
