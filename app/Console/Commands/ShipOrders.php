<?php

namespace App\Console\Commands;

use App\Helpers\MyHelper;
use App\Jobs\ProcessOrder;
use App\Models\Orders;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class ShipOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ship-orders {orderId} {--orderLine=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update order statis to completed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function validate($orderId){


        $validator = Validator::make(['orderId'=>$orderId], [
            'orderId'=>['required', 'numeric'],
        ]);

        // validate
        $result = $validator->fails();


        foreach($validator->errors()->all() as $error){
            $this->error($error);
        }

        return !$result;
     
        
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $orderId =  $this->argument('orderId');

        $orderLine = $this->option('orderLine');

        if(!$this->validate($orderId)){
           return false;
        }
        
        

        $this->info("oid...$orderId");
        
        $this->info("line...$orderLine");



        ProcessOrder::dispatch($orderId, $orderLine);

    }
}
