<?php

namespace App\Contracts;


Interface CustomerContract
{
    public function registerActivity(string $message, array $context): void;
}
