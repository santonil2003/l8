<?php

namespace App\Contracts;


Interface LogContract
{
    public function log(array $data, string $title): void;
}
