<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
        });






        $this->renderable(function (AccessDeniedHttpException $e, $request) {
            // check if route has api prefix to identify if the request type is api
            if ($request->is('api/*')) {
                /**
                 * do what ever needs to be done before sending respond...
                 */
                return response()->json([
                    'message' => $e->getMessage()
                ], 403);
            }
        });

        
        $this->renderable(function (NotFoundHttpException $e, $request) {
            // check if route has api prefix to identify if the request type is api
            if ($request->is('api/*')) {
                return response()->json([
                    'message' => $e->getMessage()
                ], 404);
            }

        });
    }
}
