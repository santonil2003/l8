<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterCustomerRequest;
use App\Models\Customer;
use App\Services\CustomerService;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response('Forbidden', Response::HTTP_FORBIDDEN);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterCustomerRequest $request)
    {

        $hashPassword = Hash::make($request->password);

        /*
        // check password
        if(Hash::check($request->password, $hashPassword)){
            return 'ok';
        }*/

        $attributes = $request->all();
        $attributes['password'] = $hashPassword;

        try {
            $customer = Customer::create($attributes);
            return $customer;
        } catch (QueryException $e) {
            $errorCode =  $e->errorInfo[1];
            if ($errorCode == 1062) {
                return response([
                    'errors' => 'Customer already exist.'
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function login(Request $request)
    {

        $customer = Customer::where('email', $request->email)->first();

        if (!is_object($customer)) {
            return response('Invalid username or password.', Response::HTTP_UNAUTHORIZED);
        }

        // check password...
        if (!Hash::check($request->password, $customer->password)) {
            return response('Invalid username or password.', Response::HTTP_UNAUTHORIZED);
        }

        $customer->token = uniqid(rand(1, 99999));

        $customer->update();

        return $customer->token;
    }


    public function list(Request $request, CustomerService $customerService)
    {

        $customerService->registerActivity('list', [$request->customer->email]);
       
        return $request->customer;
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
