<?php

namespace App\Http\Controllers\Markets;

use App\Contracts\LogContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderRequest;
use App\Models\OrderLines;
use App\Models\Orders;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
 //You will need to store the page name, IP address and time of the view.

    public function create(CreateOrderRequest $createOrderRequest){
        

        try {

            DB::beginTransaction();

            $Order =  Orders::create($createOrderRequest->all());

            $orderLines = $createOrderRequest->items;


            foreach($orderLines as $orderLineData){
                $Order->orderLines()->create($orderLineData);
            }

            DB::commit();

            $Order->items = $Order->orderLines;

            return $Order;


        } catch(\Exception $e){
            
            DB::rollBack();

            return response($e->getMessage(), 500);
        }


        
    }


    public function getOrders(LogContract $logger){


        r($_SERVER);

    
       // $logger->log($_SERVER, '');

        exit;

       $Orders = Orders::get();
       return $Orders;
    }


    public function create2(createOrderRequest $createOrderRequest){

        $Order =  Orders::create($createOrderRequest->all());
        return $Order;
    }


    public function create1(createOrderRequest $createOrderRequest){


        $Order =  new Orders();
        $Order->name = $createOrderRequest->name;
        $Order->qty = $createOrderRequest->qty;
        $Order->save();

        return $Order;
    }
}
