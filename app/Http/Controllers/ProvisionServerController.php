<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProvisionServerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
       return ['data'=>[1,2,3,4,5]];
    }
}
