<?php

namespace App\Http\Controllers;

use App\Jobs\CustomerUpdate;
use App\Jobs\ProcessOrder;
use App\Models\Customer;
use App\Models\Orders;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Aws\Sqs\SqsClient; 
use Aws\Exception\AwsException;

class TestController extends Controller
{


    public function awsTest(){
    
            $client = new SqsClient([
                'region' => env('AWS_DEFAULT_REGION', 'ap-southeast-2'),
                'version'=> '2012-11-05',
            ]);

            $params = [
                'DelaySeconds' => 0,
                'MessageAttributes' => [
                    "Title" => [
                        'DataType' => "String",
                        'StringValue' => "The Hitchhiker's Guide to the Galaxy"
                    ],
                    "Author" => [
                        'DataType' => "String",
                        'StringValue' => "Douglas Adams."
                    ],
                    "WeeksOn" => [
                        'DataType' => "Number",
                        'StringValue' => "6"
                    ]
                ],
                'MessageBody' => "Information about current NY Times fiction bestseller for week of ".date('Y-m-d H:i:s'),
                'QueueUrl' => 'https://sqs.ap-southeast-2.amazonaws.com/471970099966/TiggaQueue'
            ];
            
            try {
                $result = $client->sendMessage($params);
                var_dump($result);
            } catch (AwsException $e) {
                // output error message if fails
                error_log($e->getMessage());
            }
 
    }

    public function index( Request $request)
    {


      return $this->awsTest();

        


        // ProcessOrder::dispatch($orderId, $orderLine);


        ProcessOrder::dispatch(1,2);

        CustomerUpdate::dispatch(2,5);
return [];
        $customer = Customer::find(1);

        $roleId = 2;
        $roles = $customer->roles()->attach($roleId, ['remark'=>'remark']);


        return json_encode($customer->roles());


        

      
        $data = [
            'key' => 'value',
        ];

        // $orders = DB::select('select * from orders where deleted_at is null and qty > :qty', ['qty'=>1]);

        // DB::insert('insert into users (id, name) values (?, ?)', [1, 'Marc']);
        //$affected = DB::update('update users set votes = 100 where name = ?', ['Anita']);
        // $deleted = DB::delete('delete from users');


        // $orders = DB::table('orders')->get();

        // $orders = DB::table('orders')->pluck('name','id'); // key value pair


        /*
        DB::table('orders')->orderBy('id')->chunk(2, function($orders) {
            $items = [];
            foreach($orders as $order){
                $order->qty = rand(1,100);

                $items[] = $order;
            }

            print_r($items);
        });


        $orders = DB::table('orders')
            ->join('order_lines', 'orders.id', '=', 'order_lines.orders_id')
            ->select('orders.*', 'order_lines.item_name')
           // ->where('orders.remark', '=', 'frist order')
          //  ->whereNotNull('orders.remark')
          ->where('orders.name', 'like', 'joh%')
        //    ->whereRaw('(orders.remark IS NOT NULL) AND (orders.id = :id) AND (orders.name like :name)', ['id'=>8, 'name'=>'joh%'])
           ->paginate(15); //->get();

       return $orders;
        
*/



        /*
     $orders = Orders::orderBy('id', 'desc')->take(2)->get();
     //return $orders->refresh();
     return $orders;
  */


  /*
        $orders = Orders::orderBy('id', 'desc')->get();

        $orders = $orders->reject(function ($order) {
            return $order->qty == 10;
        });

        return $orders;
        */



        /*
        Orders::chunk(200, function($orders){
            foreach($orders as $order){
                echo "\n".$order->name;
            }
        });*/


        /*

        foreach(Orders::lazy() as $order){
            echo "\n".$order->name;
        }

        return;*/


        /**
         * // Retrieve a model by its primary key...
            $flight = Flight::find(1);

            // Retrieve the first model matching the query constraints...
            $flight = Flight::where('active', 1)->first();
         * 
         */


        return Orders::findOrFail(rand(1,20));

 // more https://laravel.com/docs/8.x/eloquent#building-queries

        //return Arr::get($data,'key1','12');

        return response($data, 201);
    }

    /**
     * login
     * @param Request $request
     * @return type {token}
     */
    public function login(Request $request)
    {

        /**
         * get post params
         */
        $email = $request->post('email');
        $password = $request->post('password');

        /**
         * @todo validate credentials from db..
         */
        if ($email != 'test@email.com' || $password != 'mypass') {
            return response('Invalid username or password.', Response::HTTP_UNAUTHORIZED);
        }


        return [
            'token' => 'newtoken'
        ];
    }

    /**
     * list users
     * @param Request $request
     * @return type array
     */
    public function users(Request $request)
    {
        return [
            ['name' => 'test1'],
            ['name' => 'test2'],
            ['name' => 'test3'],
            ['name' => 'test4'],
        ];
    }

    /**
     * list users
     * @param Request $request
     * @return type array
     */
    public function orders(Request $request)
    {
        return [
            ['order' => 'order1'],
        ];
    }
}
