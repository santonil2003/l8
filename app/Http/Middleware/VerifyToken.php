<?php

namespace App\Http\Middleware;

use App\Models\Customer;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VerifyToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $token = $request->bearerToken();

        $customer = Customer::where('token', $token)->first();

        if(!$customer || !$token){
            return response('Invalid token.', Response::HTTP_UNAUTHORIZED);
        }

        $request->customer = $customer;

        return $next($request);
    }
}
