<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;

class AppFormRequest extends FormRequest
{
    /**
     * error response
     */
    protected function failedValidation(Validator $validator)
    {
       $response = new JsonResponse(
           [
               'errors'=>$validator->errors()
           ],
           Response::HTTP_UNPROCESSABLE_ENTITY,
       );

       throw new HttpResponseException($response);
    }
}
