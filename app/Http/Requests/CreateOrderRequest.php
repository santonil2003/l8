<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CreateOrderRequest extends FormRequest
{



    protected function failedValidation1(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

    protected function failedValidation(Validator $validator)
    {

       $response = new JsonResponse(
           [
               'errors'=>$validator->errors()
           ],
           Response::HTTP_UNPROCESSABLE_ENTITY,
       );
       
       throw new HttpResponseException($response);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string', 'max:5'],
            'qty'=> ['required', 'integer'],
            'status' => ['required', 'string'],
            'remark' => ['string', 'nullable', 'max:255']
        ];
    }
}
