<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class RegisterCustomerRequest extends AppFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string', 'max:50'],
            'email'=> ['required', 'email'],
            'phone' => ['string', 'nullable', 'min:3','max:13'],
            'role'=> ['required', 'integer'],
             //'role'=> ['required', Rule::in(['ADMIN','GUEST'])],
            'password'=>['required', 'regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/']
        ];
    }

    /**
     * Override default validation error message
     *  @return array
     */
    public function messages()
    {
        return [
            'password.regex'=>'Password must be minimum eight characters, at least one letter and one number.'
        ];
    }
}
