<?php

namespace App\Jobs;

use App\Events\OnDeleteOrder;
use App\Models\Orders;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orderId;
    protected $orderLine;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($arg1, $arg2)
    {
        $this->orderId = $arg1;
        $this->orderLine = $arg2;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        Log::info('JOB process order', [$this->orderId, $this->orderLine]);
        return;
       
       $order = Orders::findOrFail($this->orderId);

       $orderLine = $order->orderLines;

       foreach($orderLine as $line){
           $line->delete();
       }

       if($order->delete()){
           OnDeleteOrder::dispatch($order);
       }


    }
}
