<?php

namespace App\Listeners;

use App\Events\OnDeleteOrder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Log\Logger;
use Illuminate\Queue\InteractsWithQueue;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnDeleteOrder  $event
     * @return void
     */
    public function handle(OnDeleteOrder $event)
    {
       
        logger()->info(date("Y-m-d"));



       logger()->info($event->order->deleted_at);

    }
}
