<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
    ];



 public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_customer', 'customer_id', 'role_id'); // return $this->belongsToMany(Role::class, 'role_user'); // return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }
    
}
