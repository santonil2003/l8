<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{

    CONST STATUS_PENDING = 'pending';
    CONST STATUS_COMPLETED = 'completed';

    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name', 
        'qty', 
        'status', 
        'remark'
    ];


    public function orderLines()
    {
        return $this->hasMany(OrderLines::class);
    }
}
