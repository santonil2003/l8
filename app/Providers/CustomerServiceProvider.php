<?php

namespace App\Providers;

use App\Contracts\CustomerContract;
use App\Services\CustomerService;
use Illuminate\Support\ServiceProvider;

class CustomerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomerContract::class, function ($app) {
            return new CustomerService(uniqid());
        });
    }

    public function provides(): array {
        return [
            CustomerContract::class
        ];
    }
}
