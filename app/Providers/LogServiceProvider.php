<?php

namespace App\Providers;

use App\Contracts\LogContract;
use App\Services\LogService;
use Illuminate\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LogContract::class, function ($app) {
            return new LogService(uniqid());
        });
    }

    public function provides(): array {
        return [
            LogContract::class
        ];
    }
}
