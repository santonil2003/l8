<?php

namespace App\Services;

use App\Contracts\CustomerContract;
use Illuminate\Support\Facades\Log;

class CustomerService implements CustomerContract
{
    public function registerActivity(string $message, array $context): void{
        Log::info($message, $context);
    }

    public function add(int $a, int $b): int{
        return $a+$b;
    }
}
