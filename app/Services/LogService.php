<?php

namespace App\Services;

use App\Contracts\LogContract;

class LogService implements LogContract
{

    protected $title;

    public function __construct($title = '')
    {
        $this->title = $title;
    }

    public function log(array $data, string $title): void{
        print_r($data);
    }
}
