<?php

namespace Database\Factories;

use App\Models\Orders;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class OrdersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Orders::class;

    protected static function newFactory()
    {
        return OrdersFactory::new();
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'tset',
            'qty' => $this->faker->numberBetween(1, 10),
            'status' => 'pending',
            'remark' => 'test',
        ];
    }
}
