<?php

use App\Http\Controllers\CustomerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Controllers\ProvisionServerController;
use App\Http\Controllers\CustomLogController;
use App\Http\Controllers\Markets\OrderController;
use App\Http\Controllers\TestController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::middleware('santo.validateToken')->get('/greet', function() {
    return ['message' => 'hellow world'];
});


/**
 * customer routes
 */
Route::get('/v1/customer', [CustomerController::class, 'index']);
Route::post('/v1/customer', [CustomerController::class, 'register']);
Route::post('/v1/customer/login', [CustomerController::class, 'login']);

Route::group(['middleware' => ['superhero.verifyToken', 'superhero.addCustomer']], function() {
    Route::get('/v1/customer/list', [CustomerController::class, 'list']);
});


/**
 * login
 */
Route::post('/v1/login', [TestController::class, 'login']);

Route::post('/v1/test', [TestController::class, 'index']);



/**
 * protected routes..
 */
Route::middleware(['middleware' => 'test.validateToken'])->group(function () {

    Route::get('/v1/users', [TestController::class, 'users']);

    Route::get('/v1/orders', [OrderController::class, 'getOrders']);


    Route::post('/v1/orders', [OrderController::class, 'create']);
});



Route::group(['middleware' => ['superhero.addCustomer']], function() {

    Route::get('/v1/url-param/{id}/{uuid}', function ($id, $uuid) {
        return [
            $id, $uuid
        ];
    })->name('url-param');
    
});



Route::match(['get', 'post'], '/v1/get-or-post', function () {
    return [
        route('url-param', ['id'=>1,'uuid'=>2]),
    ];
});

Route::any('/{any}', function($any) {
    throw new NotFoundHttpException("Invalid route /$any");
})->where('any', '.*');








