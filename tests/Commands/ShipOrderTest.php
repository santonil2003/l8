<?php

namespace Tests\Commands;

use App\Console\Commands\ShipOrders;
use App\Jobs\ProcessOrder;
use App\Models\Orders;
use Database\Factories\OrdersFactory;
use Faker\Factory;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Queue;
use Psy\Exception\TypeErrorException;
use RuntimeException;
use Tests\TestCase;
use TypeError;

class ShipOrderTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

        Queue::fake();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testInvalidArguments()
    {

        $this->expectException(RuntimeException::class);

        $this->artisan('command:ship-orders');

        Queue::assertNothingPushed(ProcessOrder::class);
    }



    public function testValidArguments()
    {
        $order = (new OrdersFactory())->create();

        $this->artisan('command:ship-orders '.$order->id);

        Queue::assertPushed(ProcessOrder::class);
    }
}
