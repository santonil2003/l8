<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUnauthorized()
    {
        $response = $this->post('/api/v1/login');


        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertSee('Invalid username or password.');
    }

    public function testInvalidLogin()
    {
        $response = $this->post('/api/v1/login', [
            'email' => 'test@email.com',
            'password' => 'mypass123'
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
        ->assertSee('Invalid username or password.');
    }

    public function testAllowLogin()
    {
        $response = $this->post('/api/v1/login', [
            'email' => 'test@email.com',
            'password' => 'mypass'
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }


}
