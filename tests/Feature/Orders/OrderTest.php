<?php

namespace Tests\Feature\Orders;

use App\Models\Orders;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testOrderInsertion()
    {
        $response = $this->post('/api/v1/orders', [
            "name" => "john",
            "qty" => "10",
            "status" => "pending",
            "remark" => "frist order",
            "items" => [
                [
                    "item_name" => "book"
                ]
            ]
        ], [
            "Authorization" => "Bearer newtoken"
        ]);

        

        $response->assertStatus(Response::HTTP_CREATED);


        // assertDatabaseMissing()
        $this->assertDatabaseHas((new Orders())->getTable(), [
            "name" => "john",
            "qty" => "10",
            "status" => "pending",
            "remark" => "frist order"
        ]);
    }
}
