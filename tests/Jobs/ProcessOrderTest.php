<?php

namespace Tests\Feature\Orders;

use App\Jobs\ProcessOrder;
use App\Models\Orders;
use ArgumentCountError;
use Database\Factories\OrdersFactory;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProcessOrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCheckIfValidParamsPassed()
    {

        $this->expectException(ArgumentCountError::class);

        ProcessOrder::dispatch();
    }


    public function testCheckIfValidOrderIdIsPassed()
    {
        //Orders::query()->forceDelete();

        $this->expectException(RuntimeException::class);

        ProcessOrder::dispatch(rand(1,1000), rand(1,1000));
    }



    public function testCheckIfOrderIdIsDeleted()
    {
        $order =  (new OrdersFactory())->create();

        ProcessOrder::dispatch($order->id, rand(1,1000));

        $this->assertSoftDeleted('orders', [
            'id'=> $order->id
        ]);
    }



}
