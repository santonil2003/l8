<?php

namespace Tests\Unit;

use App\Models\Customer;
use App\Services\CustomerService;
use Database\Factories\CustomerFactory;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->assertTrue(true);
    }

    public function test_registration(){

        
        $name = Str::random(10);
        $email = Str::random(10).'@email.com';


        $response = $this->post('/api/v1/customer', [
            "name" => $name,
            "email" => $email,
            "role" => "1",
            "password" => "Sanil123"
        ]);

    
        $response->assertStatus(Response::HTTP_CREATED);


        // assertDatabaseMissing()
        $this->assertDatabaseHas((new Customer())->getTable(), [
            "name" => $name,
            "email" => $email
        ]);
    }

    public function test_check_customer_created(){

        $customer = (new CustomerFactory())->create();

        $this->assertDatabaseHas((new Customer())->getTable(), [
            "id" => $customer->id
        ]);

    }


    public function test_customer_service_add(){
        $a = 10;
        $b = 15;

        $expectedResult = 25;
        $sum = (new CustomerService())->add($a, $b);

        $this->assertEquals($expectedResult, $sum, "Sum of $a,$b");
    }
}
